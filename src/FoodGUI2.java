import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI2 {
    private JPanel root;
    private JButton tendonButton;
    private JButton takoyakiButton;
    private JButton udonButton;
    private JButton curryriceButton;
    private JButton friedchickenButton;
    private JButton icecreamButton;
    private JTextPane list;
    private JButton checkOutButton;
    private JTextPane total;
    private JButton cancelButton;
    int sum = 0, largeprice = 0, largesum = 0, totalsum = 0;

    //クラスを使ってまとめて表示する
    void order(String food, int Price) {
        //大盛りにするか選択
        int size = JOptionPane.showConfirmDialog(
                null,
                "It's an additional ￥100, but would you like a large serving ?",
                "Order Confirmation1",
                JOptionPane.YES_NO_OPTION);
        if (size == 0) {
            JOptionPane.showMessageDialog(null, "It's a large serving of " + food);
            //大盛り100円追加
            largeprice = 100;
            JOptionPane.showMessageDialog(null, "Thank you for ordering 『  " + food + "』!!\nIt will be serving as soon as possible.");
            String currentText = list.getText();
            list.setText(currentText + food + "￥" + Price + "\n");
            sum += Price;
            largesum += largeprice;
            total.setText("SubTotal   : " + "￥" + sum + "\nLargePrice : ￥" + largesum);
        }
        if (size == 1) {
            int confirmation = JOptionPane.showConfirmDialog(
                    null,
                    "Would you like to order 『  " + food + "』?",
                    "Order Confirmation2",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation == 0) {
                JOptionPane.showMessageDialog(null, "Thank you for ordering 『  " + food + "』!!\nIt will be serving as soon as possible.");
                String currentText = list.getText();
                list.setText(currentText + food + "￥" + Price + "\n");
                sum += Price;
                total.setText("SubTotal : " + "￥" + sum + "\nLargePrice : ￥" + largesum);
            }
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI2");
        frame.setContentPane(new FoodGUI2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodGUI2() {
        //ボタンに商品画像挿入
        tendonButton.setIcon(new ImageIcon(this.getClass().getResource("Tempura.jpg")));
        takoyakiButton.setIcon(new ImageIcon(this.getClass().getResource("Takoyaki.jpg")));
        udonButton.setIcon(new ImageIcon(this.getClass().getResource("Udon.jpg")));
        curryriceButton.setIcon(new ImageIcon(this.getClass().getResource("CurryRice.jpg")));
        friedchickenButton.setIcon(new ImageIcon(this.getClass().getResource("FriedChicken.jpg")));
        icecreamButton.setIcon(new ImageIcon(this.getClass().getResource("IceCream.jpg")));
        total.setText("SubTotal : ￥" + "0" + "\nLargePrice : ￥" + largesum);
        list.setText("");
        //商品ボタンを押した後の動作
            tendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ten Don　", 500 );
            }
            });
            takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki　", 350 );
            }
            });
            udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon　", 300 );
            }
            });
            curryriceButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Curry Rice　", 450 );
                }
            });
            friedchickenButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Fried Chicken　", 400 );
                }
            });
            icecreamButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Ice Cream　", 150 );
                }
            });
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    list.setText("");
                    sum = 0;
                    largesum = 0;
                    totalsum =0;
                    total.setText("SubTotal   : ￥" + sum + "\nLargePrice : ￥" + largesum);
                }
            });
            checkOutButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    totalsum = sum + largesum;
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to order Checkout ??",
                            "Checkout Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0) {
                        list.setText("");
                        JOptionPane.showMessageDialog(null,
                                "Thank you!! \nTotal price is ￥" + totalsum);
                        sum = 0;
                        largesum = 0;
                        totalsum =0;
                        total.setText("SubTotal   : ￥" + sum + "\nLargePrice : ￥" + largesum);
                    }
                }
            });
    }
}
